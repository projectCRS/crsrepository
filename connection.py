import pymysql.cursors

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='diksha',
                             db='dw',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO `test` (`Rollno`, `name`) VALUES (%s, %s)"
        cursor.execute(sql, (10, 'Diksha Wuthoo'))

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    with connection.cursor() as cursor:
        # Read a single record
        sql = "SELECT `Rollno`, `name` FROM `test` WHERE `Rollno`=1"
        cursor.execute(sql)
        result = cursor.fetchone()
        print(result)
finally:
    connection.close()