
#text1 = card-reader is in good condition"

from textblob import TextBlob, Word
from textblob import Blobber
from textblob.taggers import NLTKTagger
import nltk
import collections
import string
##########RC and RT#############3

reviewTitle = []
reviewContent = []
noun_pairs = []
filename = "mod1.txt"
with open(filename) as f:
	for line in f:

		if line[:3] == "[t]":   #Incase the line starts with [t], then its the title of review
			reviewTitle.append(line.split("[t]")[1].rstrip("\r\n"))
		elif "##" in line:
                #Each line in review starts with '##'
				x = line.split("##")
				#x[0] is the feature given the file.Its been ignored here as its not a part of the review
				reviewContent.append(x[1].lower().rstrip("\r\n"))
#print(reviewTitle)
#print(reviewContent)

nouns= collections.defaultdict(list)
tb = Blobber(pos_tagger=NLTKTagger())
noun_dict = dict()
maxHops=5

for line in reviewContent:

    #print(line)
    x = tb(line).tags
    print(x)
    for i in range(len(x)):
     if x[i][1]=="NN" or x[i][1]=="NNS" :
        noun_word = x[i][0]
        leftHop = rightHop = -1
        #print('Noun found:', x[i][0])
        for j in range(i, len(x)):

             if (j == i + maxHops):
                break
             elif (x[j][1]=="JJ"):
                rightHop = (j - i)
                #print("Adj found to right:")
                right_adj = x[j][0]
                #print(x[j][0],rightHop)
                break
             else:
                 right_adj = ""
                 continue
        for k in range(0, i):
             # print("i",i)
             # print("j",j)

             if (k == i - maxHops):
                 break
             elif (x[k][1] == "JJ"):
                 leftHop = (i - k)
                 #print("Adj found to left:")
                 left_adj = x[k][0]
                 #print(x[k][0], leftHop)
                 break
             else:
                 left_adj = ""
                 continue
        if (leftHop > 0 and rightHop > 0):  # If adj on both sides
             if (leftHop - rightHop) >= 0:  # If left adj is farther
                 nouns[noun_word].append(right_adj)
             else:  # If right noun is farther
                 nouns[noun_word].append(left_adj)
        elif leftHop > 0:  # If noun is not found on RHS of adjective
             nouns[noun_word].append(left_adj)

        elif rightHop > 0:  # If noun is not found on LHS of adjective
            nouns[noun_word].append(right_adj)
#
from textblob import Word
from textblob import TextBlob
import nltk
from collections import OrderedDict
ordered_d = OrderedDict(sorted(nouns.items(), key=lambda x: len(x[1])))

rating_dict = dict()
for keys,values in ordered_d.items():
    print(keys)
    print(values)
    print("\n")
    polarity = 0
    k=0
    for i in values:
        polarity = polarity + TextBlob(i).sentiment.polarity
        #print("polarity: ",TextBlob(i).sentiment.polarity)
        if(TextBlob(i).sentiment.polarity > 0.0 or TextBlob(i).sentiment.polarity< 0.0):
            k=k+1
    #print("k:",k)
    if(k==0):
        rating=0
        #rating_dict[keys].append(rating)
        #print("rating: ","0")
    else:
        rating = polarity/k
        #print("rating: ",rating*4)
        #rating_dict[keys].append(rating)
    print("\n")
