import csv
import nltk
#nltk.download('punkt')
from nltk.corpus import treebank

#nltk.download('averaged_perceptron_tagger')
f = open('reviews.csv')
csv_f = csv.reader(f)

for row in csv_f:
  rev =  row[4]
  #print (row[4])
  #print(rev)
  tokens = nltk.word_tokenize(rev)
  print(tokens)
  print(nltk.pos_tag(tokens))
  from nltk.stem import PorterStemmer
  from nltk.tokenize import sent_tokenize, word_tokenize

  #nltk.download('treebank')
  #t = treebank.parsed_sents('wsj_0001.mrg')[0]
  #t.draw()
  ps = PorterStemmer()
  for w in tokens:
    print(ps.stem(w))
